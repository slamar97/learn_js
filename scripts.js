window.onload = init; // Funkcja odpalana po załadowaniu się strony

function init() {
    count();
    let countEvent = 0;
    const runTimer = setInterval(count, 1000);
    const avaibleTimer = setInterval(avaible, 1000);

    const addEventButton = document.querySelector("#addEvent");
    addEventButton.addEventListener("click", dodajWydarzenie);

    // -kubcio-
    let wszystkieWydarzeniaHTML = [];

    const mojTajmer = setInterval(() => {
        const aktualnaData = dajAktualnaDate(); // funkcja getDate ma return aktualnej daty z godziną
        sprawdzWszystkieWydarzenia(aktualnaData);
    }, 1000);

    function dajAktualnaDate() {
        return new Date();
    }

    // generalnie polecam tworzenie argumentów z funkcjami z _ na samym początku, prościej jest ją odróżnić, nie jest wymagane ale można
    function sprawdzWszystkieWydarzenia(_aktualnaData) {
        /**
         * petla po wszystkich elementach z wydarzen
         * nie jestem pewny czy złapie Ci tutaj zmienną `wszystkieWydarzeniaHTML`, jeśli nie, to podajesz ją w kolejnym argumencie czyli:
         * _aktualnaData, _wszystkieWydarzeniaHTML
         * sprawdzanie czy dany element jest ok z data podana w argumencie
         * jesli element jest nieaktualny (powininen być zaktualizowany) => zaktualizuj go
         */
        wszystkieWydarzeniaHTML.forEach((element, index) => {
            const dataWydarzenia = Date.parse(element.data);
            if (_aktualnaData > dataWydarzenia) {
                document.querySelector(`div[data-id ="${element.id}"]`).classList.remove('przed');
                document.querySelector(`div[data-id ="${element.id}"]`).classList.add('po');
            }
        });
    }

    function dodajWydarzenie() {
        // ogarnij rzeczy do zbudowania elementu wydarzenia
        // finalnie obiekt z danymi
        const dateEventInput = document.querySelector('#dateEvent');
        const dateEvent = dateEventInput.value;
        const FormatdateInput = Date.parse(dateEvent);


        const nameEventInput = document.querySelector('#nameEvent');
        const nameEvent = nameEventInput.value;
        const ileIchJuzJest = wszystkieWydarzeniaHTML.length;
        console.log(ileIchJuzJest);

        const noweWydarzenie = {
            id: ileIchJuzJest,
            nazwa: nameEvent,
            data: FormatdateInput,
            przedawnione: false
        }

        let noweNaStronie = document.createElement('div');
        noweNaStronie.className = 'przed';
        noweNaStronie.innerHTML = `${noweWydarzenie.id}. ${noweWydarzenie.nazwa} ${noweWydarzenie.data}`;
        noweNaStronie.dataset.id = ileIchJuzJest;
        document.getElementById('wydarzenia').appendChild(noweNaStronie);
        wszystkieWydarzeniaHTML.push(noweWydarzenie);
        //dodawnie do listy
        let przyciskUsuwania = document.createElement('BUTTON');
        przyciskUsuwania.id = `r${ileIchJuzJest}`;
        przyciskUsuwania.innerHTML = 'usuń';
        document.querySelector(`div[data-id ="${ileIchJuzJest}"]`).appendChild(przyciskUsuwania);

        let przyciskEdycji = document.createElement('BUTTON');
        przyciskEdycji.id = `e${ileIchJuzJest}`;
        przyciskEdycji.innerHTML = 'edytuj';
        document.querySelector(`div[data-id ="${ileIchJuzJest}"]`).appendChild(przyciskEdycji);
        /**
         * stworz nowy element na bazie obiektu i dodaj go do .wydarzenia
         * po dodaniu wydarzenia masz 2 opcje:
         * 1. przypisujesz łapiesz na nowo wszystkie wydarzenia czyli
         * wszystkieWydarzeniaHTML = document.querySelectorAll('.wydarzenia .wydarzenie'); // dlatego na poczatku dalem let
         * 2. możesz spróbować dodac jakoś ten element do istniejącej już listy wszystkieWydarzeniaHTML, ale to nie wiem jak wiec musisz sam sobie radzic
         *
         * potem przypisz ten element do konca diva .wydarzenia
         */
        // na koncu robisz
        sprawdzWszystkieWydarzenia(dajAktualnaDate());
    }

    function usunWydarzenie(_id) {
        //usunięcie wydarzenia
        //przesunięcie licznika w dół dla wydarzeń z większym id niż usuwane
        doZnikniecia = document.querySelector(`div[data-id ="${ileIchJuzJest}"]`);

    }

    function edytujWydarzenie(_id) {
        // kod
    }

    // -end kubcio-

}

function count() {
    const timezone = document.getElementById("timezone").value;
    const now = new Date(); // jeśli nie edytujesz zmiennej to robisz const a ona nie będzie się sama dynamicznie zmieniać jak będę klikać ?
    const day = now.getDay() + 1;
    const month = now.getMonth() + 1;
    const year = now.getFullYear();
    const hours = now.getHours() + parseFloat(timezone); ///#facepalm
    const minutes = now.getMinutes();
    const seconds = now.getSeconds();
    //console.log(now);

    const box = document.querySelector("#box_time");

    // box.innerHTML = (now.getDay() + 1) + "/" + () + "/" + now.getFullYear() + " || " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
    box.innerHTML = `${day} ${month} ${year} / ${hours}:${minutes}:${seconds}`;
}

function avaible() {}